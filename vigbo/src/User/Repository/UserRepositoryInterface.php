<?php

declare(strict_types=1);

/**
 * Class UserRepositoryInterface
 */
interface UserRepositoryInterface
{
    /**
     * @param int $userId
     * @return User|null
     */
    public function find(int $userId): ?User;
}
