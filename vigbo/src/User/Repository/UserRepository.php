<?php

declare(strict_types=1);

/**
 * Class MysqlUserRepository
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var Object
     */
    private $someUnitOfWork;

    /**
     * UserRepository constructor.
     * @param $someUnitOfWork
     */
    public function __construct($someUnitOfWork)
    {
        $this->someUnitOfWork = $someUnitOfWork;
    }

    /**
     * @inheritdoc
     */
    public function find(int $userId): ?User
    {
        return $someUnitOfWork->find(User::class, $userId);
    }
}
