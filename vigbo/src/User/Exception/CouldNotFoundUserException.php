<?php

declare(strict_types=1);

/**
 * Class CouldNotFoundUserException
 */
class CouldNotFoundUserException extends \Exception
{
}
