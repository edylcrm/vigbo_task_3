<?php

declare(strict_types=1);

/**
 * Class CouldNotSaveUserException
 */
class CouldNotSaveUserException extends \Exception
{
}
