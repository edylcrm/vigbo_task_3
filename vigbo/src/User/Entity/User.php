<?php

declare(strict_types=1);

/**
 * Class User
 */
class User
{
    /**
     * @var int
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
