<?php

declare(strict_types=1);

/**
 * Class CouldNotCreateArticleException
 */
class CouldNotCreateArticleException extends \Exception
{
}
