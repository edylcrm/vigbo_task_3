<?php

declare(strict_types=1);

/**
 * Class Article
 */
class Article
{
    /**
     * PRIMARY KEY
     *
     * @var int
     */
    private $id;

    /**
     * Many to one
     *
     * @var User
     */
    private $author;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Article constructor.
     * @param User $author
     * @param string $title
     * @param string $text
     */
    public function __construct(User $author, string $title, string $text)
    {
        $this->author = $author;
        $this->title = $title;
        $this->text = $text;
        $this->createdAt = new \DateTime();
    }

    /**
     * @param User $author
     * @return void
     */
    public function changeAuthor(User $author): void
    {
        $this->author = $author;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }
}
