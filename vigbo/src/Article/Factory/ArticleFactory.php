<?php

declare(strict_types=1);

/**
 * Class ArticleFactory
 */
class ArticleFactory
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * ArticleFactory constructor.
     * @param UserRepositoryInterface $userRepository
     * @param ArticleRepositoryInterface $articleRepository
     */
    public function __construct(UserRepositoryInterface $userRepository, ArticleRepositoryInterface $articleRepository)
    {
        $this->userRepository = $userRepository;
        $this->articleRepository = $articleRepository;
    }

    /**
     * @param ArticleForCreateDto $articleForCreateDto
     * @return Article
     * @throws CouldNotCreateArticleException
     * @throws CouldNotFoundUserException
     */
    public function create(ArticleForCreateDto $articleForCreateDto): Article
    {
        if (null === $author = $this->userRepository->find($articleForCreateDto->authorId)) {
            throw new CouldNotFoundUserException('User not found', 404);
        }

        /*
         * Можно добавить валидацию данных title, text на данном этапе, либо добавить валидацию в Dto в фабричный метод
         */

        $article = new Article($author, $articleForCreateDto->title, $articleForCreateDto->text);

        try {
            $this->articleRepository->save($article);
        } catch (CouldNotSaveUserException $e) {
            throw new CouldNotCreateArticleException('Could not create article', 500, $e);
        }

        return $article;
    }
}
