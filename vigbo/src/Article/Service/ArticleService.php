<?php

declare(strict_types=1);

/**
 * Class ArticleService
 */
class ArticleService
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * ArticleService constructor.
     * @param ArticleRepositoryInterface $articleRepository
     */
    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @param User $user
     * @return array
     */
    public function findUserArticles(User $user): array
    {
        return $this->articleRepository->match(new UserArticlesSpecification($user->getId()));
    }
}
