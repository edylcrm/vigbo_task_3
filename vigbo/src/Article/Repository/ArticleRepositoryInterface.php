<?php

declare(strict_types=1);

/**
 * Class ArticleRepositoryInterface
 */
interface ArticleRepositoryInterface
{
    /**
     * В данном случае предпочту реализацию persistence-oriented repository т.к. не нуждают в транзакциях
     * @param Article $article
     * @throws CouldNotSaveUserException
     */
    public function save(Article $article): void;

    /**
     * @param ArticleSpecificationInterface $specification
     * @return Article[]
     */
    public function match(ArticleSpecificationInterface $specification): array;
}
