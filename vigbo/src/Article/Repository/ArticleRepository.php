<?php

declare(strict_types=1);

/**
 * Class ArticleRepository
 */
class ArticleRepository implements ArticleRepositoryInterface
{
    /**
     * @var Object
     */
    private $someUnitOfWork;

    /**
     * ArticleRepository constructor.
     * @param $someUnitOfWork
     */
    public function __construct($someUnitOfWork)
    {
        $this->someUnitOfWork = $someUnitOfWork;
    }

    /**
     * @inheritdoc
     */
    public function save(Article $article): void
    {
        $this->someUnitOfWork->save($article);
    }

    /**
     * @param ArticleSpecificationInterface $specification
     * @return Article[]
     */
    public function match(ArticleSpecificationInterface $specification): array
    {
        $someQueryBuilder = $this->someUnitOfWork->createQueryBuilder->from(Article::class);
        $someQueryBuilder = $specification->toQueryBuilder($someQueryBuilder);

        return $someQueryBuilder->getResult();
    }
}
