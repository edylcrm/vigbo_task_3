<?php

declare(strict_types=1);

/**
 * Class ArticleForCreateDto
 */
class ArticleForCreateDto
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $text;

    /**
     * @var int
     */
    public $authorId;
}
