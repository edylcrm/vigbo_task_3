<?php

declare(strict_types=1);

/**
 * Class ArticleSpecificationInterface
 */
interface ArticleSpecificationInterface
{
    /**
     * @param $someQueryBuilder
     * @return SomeQueryBuilder
     */
    public function toQueryBuilder($someQueryBuilder): SomeQueryBuilder;
}
