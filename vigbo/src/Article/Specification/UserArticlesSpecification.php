<?php

declare(strict_types=1);

/**
 * Class UserArticlesSpecification
 */
class UserArticlesSpecification implements ArticleSpecificationInterface
{
    /**
     * @var int
     */
    private $authorId;

    /**
     * UserArticlesSpecification constructor.
     * @param int $authorId
     */
    public function __construct(int $authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @param $someQueryBuilder
     * @return SomeQueryBuilder
     */
    public function toQueryBuilder($someQueryBuilder): SomeQueryBuilder
    {
        return $someQueryBuilder->addWhere('author_ir', $this->authorId);
    }
}
